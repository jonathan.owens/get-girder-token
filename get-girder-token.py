#!/usr/bin/env python

import json
import os
from subprocess import check_output
import sys

# Allow one to specify a custom API root to use when requesting a token.
# Otherwise, we assume data.kitware.com.
if len(sys.argv) > 1:
    girder_api_root = sys.argv[1]
    print("[[INFO]] Using custom API root: %s" % girder_api_root)
else:
    girder_api_root = 'https://data.kitware.com/'
    print("[[INFO]] Using default API root: %s" % girder_api_root)

# User must have a file "key.txt" in the same directory as this script to get
# a valid Girder token. The file should be formatted simply as a valid key on
# the first line.
try:
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'key.txt'), 'r') \
            as key_file:
        key = key_file.readline().strip()
        print("[[INFO]] Successfully read key from file.")
except IOError:
    sys.exit("[[ERROR]] There is no 'key.txt' file present in this script's "
             "directory. Place a file that has a valid Girder API key on the "
             "first line alongside this script. See README.md for more "
             "information.")

call_args = ['curl', '-sX', 'POST',
             '%s/api/v1/api_key/token' % girder_api_root, '-d',
             'key=%s' % key]

out = check_output(call_args)

json_str = json.loads(out)
print(json_str['authToken']['token'])
