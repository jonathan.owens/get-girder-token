#!/usr/bin/env python

from distutils.core import setup

setup(name='Get Girder Token',
      version='1.0',
      description='Executable python file to get a Girder token using an '
                  'existing API key.',
      author='Jonathan Owens',
      author_email='jonathan.owens@kitware.com',
      scripts=['get-girder-token.py'],
      data_files=[('bin', ['key.txt'])])
