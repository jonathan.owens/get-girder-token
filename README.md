# Overview

The purpose of this repository is to provide a centralized store of a utility
to get a Girder token when one has a valid API key to that Girder instance. 
The default behavior is to request such a token from ``data.kitware.com``, 
though a custom URL root can be specified.
 
# Installation

Create a file in this directory called ``key.txt``. The line must have a 
valid API key (for whatever the URL of the Girder instance from which you 
want a token) on the first line. All other lines in the file are ignored.

Simply run ``python setup.py install`` from this root directory. The setup 
script will copy ``get-girder-token.py`` as a binary into your Python 
environment, and put ``key.txt`` alongside it.

# Usage

## Example: Default Girder URL

``$ get-girder-token.py``

Will output (given the key in ``key.txt`` is valid):

```
[[INFO]] Using default API root: https://data.kitware.com/
[[INFO]] Successfully read key from file.
$SOME_TOKEN
```

## Example: Custom Girder URL

``$ get-girder-token.py http://localhost:8080``

Will output:

```
[[INFO]] Using custom API root: http://localhost:8080
[[INFO]] Successfully read key from file.
$SOME_TOKEN
```
